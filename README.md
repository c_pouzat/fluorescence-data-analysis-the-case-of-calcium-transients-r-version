# Fluorescence data analysis - the case of calcium transients - R version

An up-to-date version of an "old classic" of mine. 

The [slides](https://drive.proton.me/urls/19PXCGYGGG#V10DuG6ONgU9) are available as well as a [book chapter](https://drive.proton.me/urls/SC7RPR7C94#nrzoUHpHcaPD) version of the material. The analysis was carried out with `Python` in these two documents, but as you will see soon, we get the same thing with `R`.

To regenerate the analysis, go to the `src` directory, start `R` and type:

```
library('rmarkdown')
rmarkdown::render("FDACCT-R.Rmd",output_dir = "../public",output_file="index.html")
```
